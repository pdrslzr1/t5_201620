package taller.estructuras;

/**
 * Created by pedrosalazar on 9/18/16.
 */
public class MinHeap<T extends Comparable<T>> implements IHeap {
    private T[] arr; //tree array
    private int N;//heap size

    //constructor
    public MinHeap() {
        arr = (T[]) new Comparable[10];
        N = 1;
    }

    //private methods as in sedgewick

    private boolean more(int i, int j) {
        return arr[i].compareTo(arr[j]) > 0;
    }

    private void exch(int i,int j) {
        T t = arr[i]; arr[i] = arr[j]; arr[j] = t;
    }

    @Override
    public void add(Object elemento) {
        if(arr.length < N+1){
            T[] n = (T[]) new Comparable[2*arr.length];
            int i = 1;
            while (arr[i]!= null){
                n[i]=arr[i];
                i++;
            }
            arr=n;
        }
        arr[++N]= (T) elemento;
        siftUp();
    }

    @Override
    public Object peek() {
        return arr[N];
    }

    @Override
    public Object poll() {
        Object a =  arr[1];
        exch(1,N--);
        arr[N+1] = null;
        siftDown();
        if(arr.length/2 > N){
            T[] n = (T[]) new Comparable[arr.length/2];
            int i = 1;
            while (arr[i]!= null){
                n[i]=arr[i];
                i++;
            }
            arr=n;
        }
        return a;
    }

    @Override
    public int size() {
        return N;
    }

    @Override
    public boolean isEmpty() {
        return N == 0;
    }

    @Override
    //swim
    public void siftUp() {
        int k = N;
        while(k > 1 && more(k/2, k)){
            exch(k/2,k);
            k = k/2;
        }
    }
    @Override
    //sink
    public void siftDown() {
        int k = 1;
        while(2*k<= N){
            int j = 2*k;
            if (j < N && more(j, j+1)) j++;
            if (!more(k, j)){
                break;
            }
            exch(k, j);
            k = j;
        }
    }

    public T[] getArray(){
        return arr;
    }
}

