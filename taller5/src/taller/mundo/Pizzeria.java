package taller.mundo;


import sun.security.krb5.internal.crypto.Des;
import taller.estructuras.MaxHeap;
import taller.estructuras.MinHeap;

import java.util.ArrayList;

public class Pizzeria
{	
	// ----------------------------------
    // Constantes
    // ----------------------------------
	
	/**
	 * Constante que define la accion de recibir un pedido
	 */
	public final static String RECIBIR_PEDIDO = "RECIBIR";
	/**
	 * Constante que define la accion de atender un pedido
	 */
	public final static String ATENDER_PEDIDO = "ATENDER";
	/**
	 * Constante que define la accion de despachar un pedido
	 */
	public final static String DESPACHAR_PEDIDO = "DESPACHAR";
	/**
	 * Constante que define la accion de finalizar la secuencia de acciones
	 */
	public final static String FIN = "FIN";
	
	// ----------------------------------
    // Atributos
    // ----------------------------------

    private class Despachables implements Comparable<Despachables>{


        private Pedido pedido;

        public Despachables(Pedido p) {
            pedido = p;
        }

        @Override
        public int compareTo(Despachables o) {
            int r = 0;
            if(pedido.getCercania() < o.getPedido().getCercania()){//comparar tiempos de entrega
                r=-1;
            }
            else{
                r=1;
            }
            return r;
        }

        public Pedido getPedido(){return pedido;}
    }
	/**
	 * Heap que almacena los pedidos recibidos
	 */
	// TODO
	MaxHeap<Pedido> pedidosRecibidos;
	/**
	 * Getter de pedidos recibidos
	 */
	// TODO
    public MaxHeap<Pedido> getPedidosRecibidos(){
        return pedidosRecibidos;
    }
 	/** 
	 * Heap de elementos por despachar
	 */
	// TODO
    MinHeap<Despachables> elementosPorDespachar;
	/**
	 * Getter de elementos por despachar
	 */
	// TODO
    public MinHeap<Despachables> getElementosPorDespachar(){
        return elementosPorDespachar;
    }
	
	// ----------------------------------
    // Constructor
    // ----------------------------------

	/**
	 * Constructor de la case Pizzeria
	 */
	public Pizzeria()
	{
		// TODO
        pedidosRecibidos = new MaxHeap<Pedido>();
        elementosPorDespachar = new MinHeap<Despachables>();
	}

	
	// ----------------------------------
    // Métodos
    // ----------------------------------
	
	/**
	 * Agrega un pedido a la cola de prioridad de pedidos recibidos
	 * @param nombreAutor nombre del autor del pedido
	 * @param precio precio del pedido 
	 * @param cercania cercania del autor del pedido 
	 */
	public void agregarPedido(String nombreAutor, double precio, int cercania)
	{
		// TODO
        Pedido p = new Pedido(precio,nombreAutor,cercania);
        pedidosRecibidos.add(p);
	}
	
	// Atender al pedido más importante de la cola
	
	/**
	 * Retorna el proximo pedido en la cola de prioridad o null si no existe.
	 * @return p El pedido proximo en la cola de prioridad
	 */
	public Pedido atenderPedido()
	{
		// TODO
        Despachables d = new Despachables((Pedido)pedidosRecibidos.peek());
		return  (Pedido)pedidosRecibidos.poll();
	}

	/**
	 * Despacha al pedido proximo a ser despachado. 
	 * @return Pedido proximo pedido a despachar
	 */
	public Pedido despacharPedido()
	{
		// TODO 
        Despachables d = (Despachables) elementosPorDespachar.poll();
        return d.getPedido();
	}
	
	 /**
     * Retorna la cola de prioridad de pedidos recibidos como un arreglo. 
     * @return arreglo de pedidos recibidos manteniendo el orden de la cola de prioridad.
     */
     public Pedido [] pedidosRecibidosList()
     {
        // TODO 
         return (Pedido[])pedidosRecibidos.getArray();
     }
     
      /**
       * Retorna la cola de prioridad de despachos como un arreglo. 
       * @return arreglo de despachos manteniendo el orden de la cola de prioridad.
       */
     public Pedido [] colaDespachosList()
     {
         // TODO 
         Despachables[] a = elementosPorDespachar.getArray();
         Pedido[] b = new Pedido[a.length];
         for (int i = 0; i < a.length;i++) {
             b[i] = a[i].getPedido();
         }
         return b;

     }
}
